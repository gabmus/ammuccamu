import express from 'express'
import compression from 'compression'
import helmet from 'helmet'
import cookieParser from 'cookie-parser'
import {PORT, UPLOADS_DIR} from './conf'
import {db_init} from './db'
import {rootRouter} from './routes/root'
import {userRouter} from './routes/user'
import {restaurantRouter} from './routes/restaurant'
import {upgradeRequestRouter} from './routes/upgrade_request'
import {orderRouter} from './routes/order'

var app: express.Application = express()

app.use(express.json())
app.use(cookieParser())
app.use(compression())
app.use(helmet({
    contentSecurityPolicy: false
}))

app.set('view engine', 'ejs');

app.use('/', express.static('public'))
app.use('/', express.static('static'))
app.use('/uploads', express.static(UPLOADS_DIR))
app.use(
    '/bootstrap-icons',
    express.static('node_modules/bootstrap-icons/font')
)
app.use(
    '/bootstrap/js',
    express.static('node_modules/bootstrap/dist/js')
)


app.use('/', rootRouter)
app.use('/user', userRouter)
app.use('/restaurant', restaurantRouter)
app.use('/upgrade_request', upgradeRequestRouter)
app.use('/order', orderRouter)


db_init(() => {
    app.listen(PORT, () => {
        console.log(`Listening on port ${PORT}`)
    })
})
