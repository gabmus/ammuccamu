import { hash_password } from '../auth'
import {db_init} from '../db'
import {User, IUser} from '../models/user'

function usage() {
    console.log(
        `Usage:      ${process.argv[0]} ${process.argv[1]} ` +
            '--username=<username> --password=<password>'
    )
}

if (process.argv.length != 4) {
    usage()
    process.exit(-1)
}

var username = process.argv[2]
var password = process.argv[3]

db_init(async () => {
    let pw_hash = await hash_password(password)
    User.create({
        username: username, pw_hash: pw_hash, is_superuser: true
    }, (err, user) => {
        if (err) {
            console.error(err)
            process.exit(-1)
        }
        else {
            console.log(`Superuser ${user.username} created`)
        }
        process.exit(0)
    })
})
