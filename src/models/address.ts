import {Request} from 'express'

// not a model per se, is included in other objects
export type AddressObj = {
    country: string,
    address: string,
    city: string,
    postal_code: string,
    intercom: string,
}

export function addressStringify(a: AddressObj): string {
    return a.country+a.address+a.city+a.postal_code+a.intercom
}

export function addressFromReq(req: Request): AddressObj {
    return {
        country: req.body.country,
        address: req.body.address,
        city: req.body.city,
        postal_code: req.body.postal_code,
        intercom: req.body.intercom,
    }
}
