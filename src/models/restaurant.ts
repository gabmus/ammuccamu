import {Schema, model, Document} from 'mongoose'
import {MenuItemObj} from './menuitem'
import {AddressObj} from './address'
import {IUser} from './user'

export interface IRestaurant extends Document {
    name: string,
    logo: string,
    image: string,
    menu: Array<MenuItemObj>,
    phone: string,
    address: AddressObj,
    owner: IUser['_id'],
}

const RestaurantSchema = new Schema<IRestaurant>({
    name: {type: String, required: true},
    logo: String,
    image: String,
    menu: [{type: {
        name: {type: String},
        ingredients: {type: String},
        notes: {type: String},
        category: {type: String},
        price: {type: Number}
    }}],
    phone: {type: String, required: true},
    address: {type: {
        country: {type: String}, address: {type: String},
        city: {type: String}, postal_code: {type: String},
        intercom: {type: String}
    }, required: true},
    owner: {type: Schema.Types.ObjectId, required: true}
})

export const Restaurant = model<IRestaurant>('Restaurant', RestaurantSchema)
