import {Schema, model, Document} from 'mongoose'
import {IUser} from './user'
import {IRestaurant} from './restaurant'
import { MenuItemObj } from './menuitem'
import { AddressObj } from './address'

export enum OrderStatus {
    SUBMITTED = 1,
    CANCELLED,
    PENDING,
    COMPLETED,
    INVALID = -1
}

const STATUS_STRING_MAP = {
    'SUBMITTED': OrderStatus.SUBMITTED,
    'CANCELLED': OrderStatus.CANCELLED,
    'PENDING': OrderStatus.PENDING,
    'COMPLETED': OrderStatus.COMPLETED,
}

export function orderStatusFromString(
        status: 'SUBMITTED' | 'CANCELLED' | 'PENDING' | 'COMPLETED'
) {
    let res = STATUS_STRING_MAP[status]
    if (res) return res
    return OrderStatus.INVALID
}

export function orderStatusToString(status: OrderStatus): string {
    switch (status) {
        case OrderStatus.SUBMITTED: return 'Submitted'
        case OrderStatus.CANCELLED: return 'Cancelled'
        case OrderStatus.PENDING: return 'Pending'
        case OrderStatus.COMPLETED: return 'Completed'
        default: return 'Invalid'
    }
}

export interface IOrder extends Document {
    restaurant: IRestaurant['_id'],
    user: IUser['_id'],
    menu_items: Array<MenuItemObj>,
    status: OrderStatus,
    submittedDateTime: Date,
    completedDateTime: Date | null,
    delivery_address: AddressObj,
    notes: string,
}

const OrderSchema = new Schema<IOrder>({
    restaurant: {type: Schema.Types.ObjectId, required: true},
    user: {type: Schema.Types.ObjectId, required: true},
    menu_items: {type: [{
        name: {type: String}, ingredients: {type: String},
        notes: {type: String}, category: {type: String},
        price: {type: Number}
    }]},
    status: {type: Number, required: true, default: OrderStatus.SUBMITTED},
    submittedDateTime: {type: Date, required: true, default: () => new Date},
    completedDateTime: {type: Date, required: false},
    delivery_address: {type: {
        country: {type: String}, address: {type: String}, city: {type: String},
        postal_code: {type: String}, intercom: {type: String}
    }, required: true},
    notes: {type: String, required: false},
})

export const Order = model<IOrder>('Order', OrderSchema)
