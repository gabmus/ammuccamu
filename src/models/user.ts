import {Schema, model, Document} from 'mongoose'
import {AddressObj} from './address'

export interface IUser extends Document {
    is_admin: boolean,  // admin = restaurant owner
    is_superuser: boolean,
    username: string,
    pw_hash: string,
    addresses: Array<AddressObj>,
}

const UserSchema = new Schema<IUser>({
    is_admin: {type: Boolean, default: false},
    is_superuser: {type: Boolean, default: false},
    username: {type: String, required: true, unique: true},
    pw_hash: {type: String, required: true},
    addresses: {type: [{
        country: {type: String}, address: {type: String},
        city: {type: String}, postal_code: {type: String},
        intercom: {type: String}
    }], required: false},
})

export const User = model<IUser>('User', UserSchema)
