import {Schema, model, Document} from 'mongoose'
import {IUser} from './user'

export interface IUpgradeRequest extends Document {
    user: IUser['_id'],
    note: string,
    open: boolean
}

const UpgradeRequestSchema = new Schema<IUpgradeRequest>({
    user: {type: Schema.Types.ObjectId, required: true},
    note: {type: String, required: true},
    open: {type: Boolean, default: true}
})

export const UpgradeRequest = model<IUpgradeRequest>('UpgradeRequest', UpgradeRequestSchema)
