import {Request} from 'express'

// not a model per se, is included in other objects
export type MenuItemObj = {
    name: string,
    ingredients: string,
    notes: string,
    category: string,
    price: number
}

export function menuItemStringify(i: MenuItemObj): string {
    return i.name + i.ingredients + i.notes + i.category + i.price
}

export function menuItemFromReq(req: Request): MenuItemObj {
    return {
        name: req.body.name,
        ingredients: req.body.ingredients,
        notes: req.body.notes,
        category: req.body.category,
        price: req.body.price,
    }
}
