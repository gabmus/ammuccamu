import dotenv from 'dotenv'

dotenv.config()

export const PORT = process.env.AMC_PORT || 3000
export const DB = process.env.AMC_DB || 'mongodb://127.0.0.1:27017/amc'
export const APPNAME = process.env.AMC_APPNAME || 'Ammucamu'
export const APPDESC = process.env.AMC_APPDESC || 'Your Favorite Food Delivery'
export const SITEURL = process.env.AMC_SITEURL || 'http://localhost:3000'

export const CTX = {
    appname: APPNAME,
    appdesc: APPDESC,
    siteurl: SITEURL
}

// based on OWASP cheat sheet recommendations (as of March, 2022)
export const HASH_CONF = {
    parallelism: 1,
    memoryCost: 64000, // 64 mb
    timeCost: 3 // number of itetations
}

let _secret = process.env.SECRET
if (!_secret) {
    console.error('SECRET environment variable is not set. Exiting.')
    process.exit(1)
}
export const SECRET: string = _secret

export const JWT_EXPIRATION_MS = 259200000  // 259200ms = 72h = 3d

export const UPLOADS_DIR = 'uploads'
export const RESTAURANT_IMG_DIR = `${UPLOADS_DIR}/restaurant`
