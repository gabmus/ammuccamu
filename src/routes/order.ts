import {Router, Response, Request} from 'express';
import {get_user_from_jwt, get_user_model, UserMinimal} from '../auth'
import {CTX} from '../conf';
import {IOrder, Order, OrderStatus, orderStatusFromString, orderStatusToString} from '../models/order'
import {IRestaurant, Restaurant} from '../models/restaurant';


export var orderRouter: Router = Router()

orderRouter.post('/', (req, res) => {
    get_user_model(req, (user) => {
        if (!user) return res.sendStatus(401)
        Restaurant.findById(req.body.restaurant_id).then(rst => {
            if (!rst) return res.sendStatus(404)
            let order = {
                restaurant: rst._id.toString(),
                user: user._id.toString(),
                menu_items: req.body.menu_items,
                status: OrderStatus.SUBMITTED,
                submittedDateTime: new Date(),
                completedDateTime: null,
                delivery_address: req.body.delivery_address,
                notes: req.body.notes
            }
            if (order.menu_items.length <= 0) return res.sendStatus(400)
            if (order.delivery_address.postal_code != rst.address.postal_code) {
                return res.sendStatus(400)
            }
            Order.create(order).then(nOrder => {
                res.send(JSON.stringify(nOrder))
            }).catch(err => {
                console.error(err)
                res.sendStatus(500)
            })
        }).catch(err => {
            console.error(err)
            res.sendStatus(500)
        })
    })
})

orderRouter.get('/', (req, res) => {
    get_user_from_jwt(req, (user) => {
        if (!user) return res.redirect('/')
        let ctx: any = {...CTX}
        ctx.user = user
        Order.find({user: user._id.toString()}).sort('-submittedDateTime').then(orders => {
            ctx.orders = orders || []
            ctx.orders.forEach((order: any, i: number) => {
                ctx.orders[i].order_status = orderStatusToString(order.status)
            })
            ctx.pagetitle = 'Order History'
            res.render('pages/orders', ctx)
        }).catch(err => {
            console.error(err)
            res.redirect('/')
        })
    })
})

function get_order_context(
        req: Request,
        cb: (order: IOrder, restaurant: IRestaurant, user: UserMinimal) => any,
        onerr: (err: any) => any
): void {
    get_user_from_jwt(req, (user) => {
        if (!user) return onerr('no user')
        Order.findById(req.params._id).then(order => {
            if (!order) return onerr('no order')
            Restaurant.findById(order.restaurant).then(rst => {
                if (!rst) {
                    return onerr('no restaurant')
                }
                if (!(
                        order.user.toString() == user._id.toString() ||
                        rst.owner.toString() == user._id.toString() ||
                        user.is_superuser
                )) {
                    return onerr('not authorized to access this order')
                }
                cb(order, rst, user)
            }).catch(err => {
                console.error(err)
                onerr(err)
            })
        }).catch(err => {
            console.error(err)
            onerr(err)
        })
    })
}

orderRouter.get('/:_id', (req, res) => {
    get_order_context(req, (order, rst, user) => {
        let ctx: any = {...CTX}
        ctx.user = user
        ctx.restaurant = rst
        ctx.order = order
        ctx.order_status = orderStatusToString(order.status)
        ctx.pagetitle = `Order to ${rst.name}`
        res.render('pages/order', ctx)
    }, (err) => {
        res.redirect('/')
    })
})

orderRouter.get('/:_id/status', (req, res) => {
    get_order_context(req, (order, rst, user) => {
        res.send(JSON.stringify({
            status: orderStatusToString(order.status)
        }))
    }, (err) => {
        res.sendStatus(500)
    })
})

orderRouter.put('/:_id/status', (req, res) => {
    get_order_context(req, (order, rst, user) => {
        if (!(
                user.is_superuser ||
                (user.is_admin && user._id.toString() == rst.owner)
        )) {
            return res.sendStatus(401)
        }
        let status = orderStatusFromString(req.body.status)
        if (status == OrderStatus.INVALID) return res.sendStatus(400)
        if ([OrderStatus.COMPLETED, OrderStatus.CANCELLED].includes(order.status)) {
            return res.sendStatus(400)
        }
        order.status = status
        order.save()
        res.sendStatus(200)
    }, (err) => {
        res.sendStatus(500)
    })
})
