import {Router} from 'express'
import {APPDESC, APPNAME, CTX} from '../conf'
import {get_user_from_jwt} from '../auth'
import { IRestaurant, Restaurant } from '../models/restaurant'
import { CallbackError } from 'mongoose'

export var rootRouter: Router = Router()

rootRouter.get('/', (req, res) => {
    get_user_from_jwt(req, (user) => {
        let ctx: any = {...CTX}
        if (user) ctx.user = user
        ctx.preloadFiles = [...Array(5).keys()].map(
            i => `/img/jumbotron/${i}.webp`
        )
        res.render('pages/index', ctx)
    })
})

rootRouter.get('/search', (req, res) => {
    let term = req.query.searchterm as string
    if (!term) res.redirect('/')
    get_user_from_jwt(req, (user) => {
        let ctx: any = {...CTX}
        if (user) ctx.user = user
        ctx.restaurants = []
        ctx.searchterm = term
        Restaurant.find().or([
            {name: new RegExp(term, 'i')},
            {'address.postal_code': term},
            {'address.city': new RegExp(term, 'i')},
        ]).then((restaurants: Array<IRestaurant>) => {
            ctx.restaurants = restaurants
            ctx.preloadFiles = restaurants.map(r => r.logo)
            ctx.pagetitle = `Restaurants in ${term}`
            res.render('pages/search', ctx)
        }).catch(err => {
            console.error(err)
            res.sendStatus(500)
        })
    })
})

rootRouter.get('/manifest.json', (req, res) => {
    res.contentType('application/json')
    res.send(JSON.stringify({
        '$schema': 'https://json.schemastore.org/web-manifest-combined.json',
        name: APPNAME,
        short_name: APPNAME,
        start_url: '/',
        display: 'standalone',
        background_color: '#e9ecef',
        theme_color: '#26a269',
        description: APPDESC,
        icons: [
            {
                src: '/maskable512.png',
                sizes: '512x512',
                type: 'image/png',
                purpose: 'maskable'
            },
            {
                src: '/logo512.png',
                sizes: '512x512',
                type: 'image/png',
                purpose: 'any'
            }
        ]
    }))
})
