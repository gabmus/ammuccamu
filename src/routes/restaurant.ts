import {Request, Response, Router} from 'express'
import {get_user_from_jwt, get_user_model, UserMinimal} from '../auth'
import {CTX, RESTAURANT_IMG_DIR} from '../conf'
import multer from 'multer'
import {IRestaurant, Restaurant} from '../models/restaurant'
import {CallbackError, SortOrder} from 'mongoose'
import fs from 'fs'
import {addressFromReq, AddressObj} from '../models/address'
import {menuItemFromReq, menuItemStringify} from '../models/menuitem'
import { Order, orderStatusToString } from '../models/order'

export var restaurantRouter: Router = Router()
var upload = multer({dest: RESTAURANT_IMG_DIR})  // TODO filename as file hash?

restaurantRouter.get('/new', (req, res) => {
    get_user_model(req, (user) => {
        if (!user || !(user.is_admin || user.is_superuser)) return res.redirect('/')
        let ctx: any = {...CTX}
        ctx.user = {
            _id: user._id,
            username: user.username,
            is_admin: user.is_admin,
            is_superuser: user.is_superuser
        }
        ctx.pagetitle = 'New Restaurant'
        res.render('pages/restaurant/new', ctx)
    })
})

function manageRestaurantUpload(req: Request, res: Response, field: 'logo' | 'image') {
    var deleteFile = () => {
        if (req.file && fs.existsSync(req.file.path)) {
            fs.unlinkSync(req.file.path)
        }
    }
    get_user_model(req, (user) => {
        if (!user || !(user.is_admin || user.is_superuser)) {
            deleteFile()
            return res.sendStatus(401)
        }
        Restaurant.findById(req.params._id, (err: CallbackError, restaurant: IRestaurant) => {
            if (err) {
                console.error(err)
                deleteFile()
                return res.sendStatus(500)
            }
            if (!restaurant) {
                deleteFile()
                return res.sendStatus(404)
            }
            if (!(
                    user.is_superuser || (
                        user._id.toString() == restaurant.owner.toString()
                    )
            )) {
                deleteFile()
                return res.sendStatus(401)
            }
            if (!req.file) return res.sendStatus(400)
            let prev = restaurant[field]
            if (prev) {
                prev = prev.substring(1)
                if (fs.existsSync(prev)) {
                    fs.unlinkSync(prev)
                }
            }
            restaurant[field] = `/${RESTAURANT_IMG_DIR}/${req.file.filename}`
            restaurant.save()
            res.send(restaurant[field])
        })
    })
}

restaurantRouter.post('/', (req, res) => {
    get_user_model(req, (user) => {
        if (!user || !(user.is_admin || user.is_superuser)) {
            return res.sendStatus(401)
        }
        let newRestaurant = {
            name: req.body.name,
            phone: req.body.phone,
            address: addressFromReq(req),
            owner: user._id.toString()
        }
        Restaurant.create(newRestaurant, (e, restaurant) => {
            if (e) {
                console.error(e)
                return res.sendStatus(500)
            }
            res.send(JSON.stringify(restaurant))
        })
    })
})

function get_user_and_restaurant(
        req: Request, res: Response,
        cb: (user: UserMinimal | null, restaurant: IRestaurant) => any
) {
    get_user_from_jwt(req, (user) => {
        Restaurant.findById(req.params._id, (err: CallbackError, restaurant: IRestaurant) => {
            if (err) return res.sendStatus(500)
            if (!restaurant) return res.sendStatus(404)
            cb(user, restaurant)
        })
    })
}

function get_restaurant_context(req: Request, res: Response, cb: (ctx: any) => any) {
    let ctx: any = {...CTX}
    get_user_and_restaurant(req, res, (user, restaurant) => {
        ctx.restaurant = restaurant
        if (user) ctx.user = user
        cb(ctx)
    })
}

restaurantRouter.get('/:_id', (req, res) => {
    get_restaurant_context(req, res, (ctx) => {
        ctx.preloadFiles = [ctx.restaurant.logo, ctx.restaurant.image]
        ctx.pagetitle = ctx.restaurant.name
        res.render('pages/restaurant/restaurant', ctx)
    })
})

restaurantRouter.get('/:_id/edit', (req, res) => {
    get_restaurant_context(req, res, (ctx) => {
        if (!(
                ctx.user &&
                (
                    ctx.user.is_superuser ||
                    (ctx.user._id.toString() == ctx.restaurant.owner.toString())
                )
        )) return res.sendStatus(401)
        ctx.pagetitle = `Edit ${ctx.restaurant.name}`
        res.render('pages/restaurant/edit', ctx)
    })
})

restaurantRouter.put('/:_id', (req, res) => {
    get_user_and_restaurant(req, res, (user, restaurant) => {
        if (!(
                user && (
                    user.is_superuser || (
                        user.is_admin &&
                        user._id.toString() == restaurant.owner.toString()
                    )
                )
        )) return res.sendStatus(401)
        restaurant.update({
            name: req.body.name, phone: req.body.phone,
            address: addressFromReq(req)
        }, (e: CallbackError) => {
            if (e) return res.sendStatus(500)
            restaurant.save()
            res.sendStatus(200)
        })
    })
})

restaurantRouter.post('/:_id/logo', upload.single('logo'), (req, res) => {
    manageRestaurantUpload(req, res, 'logo')
})

restaurantRouter.post('/:_id/image', upload.single('image'), (req, res) => {
    manageRestaurantUpload(req, res, 'image')
})

restaurantRouter.post('/:_id/menu', (req, res) => {
    get_user_and_restaurant(req, res, (user, restaurant) => {
        if (!(
                user &&
                (
                    user.is_superuser ||
                    (user._id.toString() == restaurant.owner.toString())
                )
        )) return res.sendStatus(401)
        restaurant.menu.push({
            name: req.body.name,
            ingredients: req.body.ingredients,
            notes: req.body.notes,
            category: req.body.category,
            price: req.body.price
        })
        restaurant.save()
        res.sendStatus(200)
    })
})

restaurantRouter.delete('/:_id/menu', (req, res) => {
    get_user_and_restaurant(req, res, (user, restaurant) => {
        if (
                !user ||
                (
                    !user.is_superuser ||
                    (user._id.toString() != restaurant.owner.toString())
                )
        ) return res.sendStatus(401)
        let target = menuItemStringify(menuItemFromReq(req))
        let found = false
        restaurant.menu.forEach((item, i) => {
            console.log(i)
            console.log(menuItemStringify(item), target)
            if (menuItemStringify(item) == target) {
                found = true
                restaurant.menu.splice(i, 1)
                restaurant.save()
                return res.sendStatus(200)
            }
        })
        if (!found) res.sendStatus(404)
    })
})

restaurantRouter.get('/:_id/orders', (req, res) => {
    get_user_and_restaurant(req, res, (user, restaurant) => {
        if (!user || !(user.is_superuser || (
                user.is_admin &&
                restaurant.owner.toString() == user._id.toString()
        ))) {
            return res.redirect('/')
        }
        let ctx: any = {...CTX}
        ctx.user = user
        ctx.restaurant = restaurant
        Order.find({restaurant: restaurant._id.toString()}).sort('-submittedDateTime').then(orders => {
            ctx.orders = orders || []
            ctx.orders.forEach((order: any, i: number) => {
                ctx.orders[i].order_status = orderStatusToString(order.status)
            })
            ctx.pagetitle = `Orders to ${ctx.restaurant.name}`
            res.render('pages/restaurant/orders', ctx)
        }).catch(err => {
            console.error(err)
            res.redirect('/')
        })
    })
})
