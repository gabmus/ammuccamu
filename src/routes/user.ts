import {Router} from 'express'
import {gen_crud, CRUD_ERR, CRUD_UNAUTH, CRUD_BAD, CRUD_OK, CRUD_CREATED} from '../routes/crud'
import {IUser, User} from '../models/user'
import {generate_jwt, get_user_model, hash_password, verify_password} from '../auth'
import {CallbackError} from 'mongoose'
import {CTX, JWT_EXPIRATION_MS} from '../conf'
import {IRestaurant, Restaurant} from '../models/restaurant'
import {addressFromReq, AddressObj, addressStringify} from '../models/address'

export var userRouter: Router = Router()

userRouter.use(gen_crud<IUser>(User, {
    create_mdw: async (req, res) => {
        req.body.pw_hash = await hash_password(req.body.password)
        req.body.is_superuser = false
        req.body.is_admin = false
        return CRUD_CREATED
    },
    remove_mdw: (req, res) => CRUD_ERR,
    update_mdw: async (req, res) => CRUD_BAD,
    readOne_mdw: (req, res) => CRUD_ERR,
    readMany_mdw: async (req, res) => {
        if (false) {  // check if superuser
            return CRUD_OK
        }
        return CRUD_UNAUTH
    },
}))

const AUTH_COOKIE = 'jwt'

userRouter.post('/login', (req, res) => {
    User.findOne({username: req.body.username}, (e: CallbackError, user: IUser) => {
        if (e) {
            console.error(e)
            return res.sendStatus(500)
        }
        if (!user) {
            return res.sendStatus(401)
        }
        verify_password(req.body.password, user.pw_hash).then(
            ok => {
                if (ok) {
                    res.cookie(AUTH_COOKIE, generate_jwt(user._id.toString()), {
                        maxAge: JWT_EXPIRATION_MS
                    })
                    res.send({sucecss: true})
                }
                else {
                    res.sendStatus(500)
                }
            }
        )
    })
})

userRouter.post('/logout', (req, res) => {
    res.cookie(AUTH_COOKIE, '')
    res.sendStatus(200)
})

userRouter.get('/account', (req, res) => {
    get_user_model(req, (user) => {
        if (!user) return res.redirect('/')
        let ctx: any = {...CTX}
        ctx.pagetitle = `${user.username} User Account`
        if (user.is_admin || user.is_superuser) {
            Restaurant.find(
                {owner: user._id.toString()},
                (err: CallbackError, restaurants: Array<IRestaurant>) => {
                    if (err) {
                        console.error(err)
                        return res.sendStatus(500)
                    }
                    if (!restaurants) restaurants = []
                    ctx.user = {
                        _id: user._id,
                        username: user.username,
                        addresses: user.addresses,
                        is_admin: user.is_admin,
                        is_superuser: user.is_superuser
                    }
                    ctx.restaurants = restaurants
                    res.render('pages/user', ctx)
                }
            )
        }
        else {
            ctx.user = {
                _id: user._id,
                username: user.username,
                addresses: user.addresses,
                is_admin: user.is_admin,
                is_superuser: user.is_superuser,
                restaurants: []
            }
            res.render('pages/user', ctx)
        }
    })
})

userRouter.put('/password', (req, res) => {
    get_user_model(req, (user) => {
        if (!user) return res.sendStatus(500)
        verify_password(req.body.currentPassword, user.pw_hash).then(async (ok) => {
            if (ok) {
                user.pw_hash = await hash_password(req.body.newPassword)
                user.save()
                res.sendStatus(200)
            }
            else {
                res.sendStatus(500)
            }
        })
    })
})

userRouter.post('/address', (req, res) => {
    get_user_model(req, (user) => {
        if (!user) return res.sendStatus(500)
        var newAddr: AddressObj = addressFromReq(req)
        user.addresses.push(newAddr)
        user.save()
        res.sendStatus(201)
    })
})

userRouter.delete('/address', (req, res) => {
    get_user_model(req, (user) => {
        if (!user) return res.sendStatus(400)
        let target = addressStringify(addressFromReq(req))
        let found = false
        user.addresses.forEach((addr, i) => {
            if (addressStringify(addr) == target) {
                found = true
                user.addresses.splice(i, 1)
                user.save()
                return res.sendStatus(200)
            }
        })
        if (!found) res.sendStatus(404)
    })
})
