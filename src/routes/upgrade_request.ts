import {Request, Response, Router} from 'express'
import {IUpgradeRequest, UpgradeRequest} from '../models/upgrade_request'
import {IUser, User} from '../models/user'
import {get_user_from_jwt, get_user_model, UserMinimal} from '../auth'
import {CTX} from '../conf'

export var upgradeRequestRouter: Router = Router()

upgradeRequestRouter.get('/', (req, res) => {
    get_user_model(req, (user) => {
        if (!user || !user.is_superuser) return res.redirect('/')
        let ctx: any = {...CTX}
        ctx.user = {
            _id: user._id,
            username: user.username,
            is_admin: user.is_admin,
            is_superuser: user.is_superuser
        }
        ctx.pagetitle = 'User Upgrade Requests'
        UpgradeRequest.find({open: true}).then(async (ureqs) => {
            let ureqs_w_users = await Promise.all(ureqs.map(async ureq => {
                let ruser = await User.findById(ureq.user).exec()
                if (!ruser) return null
                let nureq = {
                    user: {
                        username: ruser.username,
                        _id: ruser._id.toString()
                    },
                    note: ureq.note,
                    _id: ureq._id.toString()
                }
                return nureq
            }))
            ureqs_w_users = ureqs_w_users.filter(ureq => !!ureq)
            ctx.upgrade_requests = ureqs_w_users
            res.render('pages/upgrade_request/index', ctx)
        }).catch(err => {
            console.error(err)
            res.sendStatus(500)
        })
    })
})

upgradeRequestRouter.post('/', (req, res) => {
    get_user_model(req, (user) => {
        if (!user || user.is_superuser || user.is_admin) return res.sendStatus(401)
        if (!req.body.note) return res.sendStatus(400)
        UpgradeRequest.create({
            user: user._id,
            note: req.body.note,
            open: true,
        }).then(ureq => {
            res.sendStatus(200)
        }).catch(err => {
            console.error(err)
            res.sendStatus(500)
        })
    })
})

upgradeRequestRouter.get('/new', (req, res) => {
    get_user_model(req, (user) => {
        if (!user || user.is_superuser || user.is_admin) return res.redirect('/')
        let ctx: any = {...CTX}
        ctx.user = {
            _id: user._id,
            username: user.username,
            is_admin: user.is_admin,
            is_superuser: user.is_superuser
        }
        ctx.pagetitle = 'Request Account Upgrade'
        res.render('pages/upgrade_request/new', ctx)
    })
})

upgradeRequestRouter.post('/:_id', (req, res) => {
    get_user_model(req, (user) => {
        if (!user || !user.is_superuser) return res.sendStatus(401)
        UpgradeRequest.findOne({open: true, _id: req.params._id}).then(ureq => {
            if (!ureq) return res.sendStatus(404)
            User.findById(ureq.user).then(user => {
                if (!user) return res.sendStatus(404)
                user.is_admin = req.body.approved
                user.save()
                ureq.open = false
                ureq.save()
                res.sendStatus(200)
            }).catch(err => {
                console.error(err)
                res.sendStatus(500)
            })
        }).catch(err => {
            console.error(err)
            res.sendStatus(500)
        })
    })
})
