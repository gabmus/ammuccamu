import {CallbackError, Document, Model} from 'mongoose'
import {Router, Request, Response} from 'express'

export type MdwRes = {ok: boolean, status: number}
export type CrudMdw = (req: Request, res: Response) => MdwRes | Promise<MdwRes>

export type CrudOpts = {
    create_mdw?: CrudMdw,
    readMany_mdw?: CrudMdw,
    readOne_mdw?: CrudMdw,
    update_mdw?: CrudMdw,
    remove_mdw?: CrudMdw,
}

export const CRUD_ERR = {ok: false, status: 500}
export const CRUD_BAD = {ok: false, status: 400}
export const CRUD_UNAUTH = {ok: false, status: 401}
export const CRUD_OK = {ok: true, status: 200}
export const CRUD_CREATED = {ok: true, status: 201}

export type CrudMdwNames = 'create_mdw' | 'readMany_mdw' | 'readOne_mdw' | 'update_mdw' | 'remove_mdw'

export function gen_crud<T extends Document>(model: Model<T>, opts: CrudOpts) {

    const mdw = async (
            name: CrudMdwNames, req: Request, res: Response, _default: MdwRes,
            on_ok: (status: number) => void
    ) => {
        let fun: CrudMdw | undefined = opts[name]
        let mdw_res: MdwRes = fun ? await fun(req, res) : _default
        if (mdw_res.ok) on_ok(mdw_res.status)
        else res.sendStatus(mdw_res.status)
    }

    const handle_err = (e: CallbackError, res: Response) => {
        console.error(e)
        res.sendStatus(500)
    }
    
    const create = (req: Request, res: Response) => {
        mdw('create_mdw', req, res, CRUD_CREATED, (status) => {
            const newEntry = req.body
            model.create(newEntry, (e, entry) => {
                if (e) {
                    handle_err(e, res)
                }
                else {
                    // res.statusCode = status
                    // res.send(newEntry)
                    //// avoid sending sensitive info like the pw hash
                    res.sendStatus(status)
                }
            })
        })
    }

    const readMany = (req: Request, res: Response) => {
        mdw('readMany_mdw', req, res, CRUD_OK, (status) => {
            let query = res.locals.query || {}
            model.find(query, (e: CallbackError, result: Array<T>) => {
                if (e) {
                    handle_err(e, res)
                }
                else {
                    res.statusCode = status
                    res.send(result)
                }
            })
        })
    }

    const readOne = (req: Request, res: Response) => {
        mdw('readOne_mdw', req, res, CRUD_OK, (status) => {
            const {_id} = req.params
            model.findById(_id, (e: CallbackError, result: T) => {
                if (e) {
                    handle_err(e, res)
                } else {
                    res.statusCode = status
                    res.send(result)
                }
            })
        })
    }

    const update = (req: Request, res: Response) => {
        mdw('update_mdw', req, res, CRUD_OK, (status) => {
            const changedEntry = req.body
            model.update({_id : req.params._id}, {$set : changedEntry}, (e: CallbackError) => {
                if (e)
                    res.sendStatus(500)
                else
                    res.sendStatus(status)
            })
        })
    }

    const remove = (req: Request, res: Response) => {
        mdw('remove_mdw', req, res, CRUD_OK, (status) => {
            model.remove({_id : req.params._id}, (e) => {
                if (e)
                    res.status(500).send(e)
                else
                    res.sendStatus(status)
            })
        })
    }


    let router: Router = Router()

    router.post('/', create)
    router.get('/', readMany)
    router.get('/id/:_id', readOne)
    router.put('/id/:_id', update)
    router.delete('/id/:_id', remove)

    return router
}
