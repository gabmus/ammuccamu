import {connect} from 'mongoose'
import {DB} from './conf'

export function db_init(cb: () => any) {
    console.log('Connecting to database', DB, '...')
    connect(DB).then(() => {
        console.log('Connected to database')
        cb()
    }).catch(err => {
        console.error('Error connecting to database', DB, ':', err)
        process.exit(1)
    })
}
