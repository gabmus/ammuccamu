import argon2 from 'argon2';
import crypto from 'crypto';
import {HASH_CONF, SECRET, JWT_EXPIRATION_MS} from './conf'
import {sign, verify} from 'jsonwebtoken'
import {Request} from 'express';
import {IUser, User} from './models/user';
import {AddressObj} from './models/address';
import {CallbackError} from 'mongoose';

 
export async function hash_password(password: string): Promise<string> {
    let salt = crypto.randomBytes(16);
    return await argon2.hash(password, {
        ...HASH_CONF,
        salt,
    })
}
 
export async function verify_password(
        password: string, hash: string
): Promise<boolean> {
    return await argon2.verify(hash, password, HASH_CONF);
}

export function generate_jwt(_id: string): string {
    return sign({_id: _id}, SECRET, {expiresIn: JWT_EXPIRATION_MS})
}

export type UserMinimal = {
    _id: string, username: string, addresses: Array<AddressObj>,
    is_superuser: boolean, is_admin: boolean
}

export function get_user_from_jwt(req: Request, cb: (user: UserMinimal | null) => any): void {
    const token = req.cookies.jwt
    if (!token) return cb(null)
  
    verify(token, SECRET, (err: any, decoded: any) => {
        if (err || !decoded) return cb(null)
        User.findById(decoded._id, (err: any, user: IUser) => {
            if (err || !user) return cb(null)
            cb({
                _id: user._id,
                username: user.username,
                addresses: user.addresses,
                is_superuser: user.is_superuser,
                is_admin: user.is_admin
            })
        })
    })
}

export function get_user_model(req: Request, cb: (user: IUser | null) => any) {
    const token = req.cookies.jwt
    if (!token) return cb(null)
  
    verify(token, SECRET, (err: any, decoded: any) => {
        if (err || !decoded) return cb(null)
        User.findById(decoded._id, (e: any, user: IUser) => {
            if (e || !user) return cb(null)
            cb(user)
        })
    })
}
