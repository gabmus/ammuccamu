\documentclass[a4paper,12pt]{article}
\usepackage[tuenc]{fontspec}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{array}
\usepackage{graphicx}
\usepackage{color}
\usepackage{caption}
\usepackage{subcaption}  % 2x1 figures
\usepackage{xcolor}
\usepackage{url}
\usepackage{textcomp}
\usepackage{parskip}
\usepackage[pdfborder=0]{hyperref}  % links
\usepackage{float}  % float images in this exact point
\usepackage{minted}  % code w/ syntax

\usepackage{etoolbox}

% don't highlight pygments errors
\makeatletter
\AtBeginEnvironment{minted}{\dontdofcolorbox}
\def\dontdofcolorbox{\renewcommand\fcolorbox[4][]{##4}}
\makeatother

\usepackage[english]{babel}
\usepackage[backend=biber,style=numeric]{biblatex}

\bibliography{report}

\newcommand{\AppN}{\textit{Ammuccamu} }

\title{%
    \AppN: A Food Delivery Web App \\
    \large Corso di Programmazione Web e Mobile \\
    A.A. 2021-2022
}
\author{Gabriele Musco, 945786}
\date{\today}

\begin{document}

\maketitle

{\footnotesize\begin{center}
    \textit{GitLab repository: \url{https://gitlab.com/gabmus/ammuccamu}}
\end{center}}

\tableofcontents

\pagebreak

\section{Introduction}

\AppN is a proof of concept for a food delivery web application. Restaurant owners can register their businesses, allowing users to easily find them based on the city or postal code, and make delivery orders right from their devices.

\subsection{Beneficiaries}

The beneficiaries for \AppN are of two kinds:

\begin{itemize}
    \item Restaurant owners looking to offer their potential customers a way to discover their business, and offering existing ones a simple way to make their orders;
    \item People looking for quick and easy ways to order food from their favorite restaurants, or looking for something different to try.
\end{itemize}

The user interface is familiar and straight forward, offering the right blend of visual appeal as well as usability.

Using \AppN does not require any technical knowledge from either side.

\subsection{Value and Business Model}

\AppN is free software, licensed under the terms of the \textit{GNU Affero General Public License Version 3}\cite{agpl3}; therefore its code is completely free to use, share, adapt, modify and redistribute.

The proposed business model consists in keeping the application completely free and self-hostable (this would require technical knowledge for both hosting and maintaining the service), but at the same time offering a paid SaaS (\textit{Software as a Service})\cite{saas} option.

This would offer business owners the possibility to pay for a custom managed instance, adapted to fit their branding as well as their particular needs, with price scaling depending on the allocated infrastructural resources and the amount of customization requested.

Since the application allows for hosting multiple restaurants in one instance, this could open the possibility for big chain restaurants to share a single instance, or multiple business owners in the same area coming together to pay for a shared instance.

This commercial SaaS option would provide the necessary funding needed for further development and maintenance, to the benefit of developers, users and business owners alike.

Additionally, the option to pay a flat and potentially small fee (particularly compared to the main competitors), with the added possibility of rebranding the experience to fit the restaurant's brand identity, could be a deciding factor in preferring \AppN over the competition.

\subsection{Technical Choices}

\AppN is built using the following technologies:

\begin{itemize}
    \item \textbf{Node}: Server-side JavaScript engine that runs the backend code.
    \item \textbf{TypeScript}: Superset of JavaScript, offering optional static typing and improving on the development experience and maintainability. The \texttt{strict} flag is used, enabling a wide range of type checking behavior that results in stronger guarantees of program correctness\cite{ts-strict}. TypeScript code is transpiled to JavaScript, so it's fully compatible with existing JavaScript runtimes, libraries and other technologies.
    \item \textbf{Express}: Node library offering tools to create a web application backend. This is used to define both templating and REST routes.
    \item \textbf{MongoDB}: NoSQL document based database, used to store the application data.
    \item \textbf{Mongoose}: Node library offering an ODM (\textit{Object Document Mapper})\cite{odm} interface on top of MongoDB.
    \item \textbf{EJS}: JavaScript templating library, used as an Express middleware.
    \item \textbf{SCSS}: CSS extension language, provides extra features and ease of use. It's transpiled to CSS.
    \item \textbf{Bootstrap}: Frontend toolkit providing pre-built web widgets and styles via CSS and JavaScript.
\end{itemize}

The frontend is delivered via server-side templating, while most interactions users have in a single page are carried out using asynchronous REST calls to the backend (using the fetch API\cite{fetch}).

User password are stored as salted hashes, following modern security practices.

Authentication is done through the REST API. Upon successful authentication, the server sends the client a JSON Web Token (JWT)\cite{jwt} signed using a secret key that is not stored in the code repository. This secret can be passed to the server as an environment variable at runtime.

User carts are saved per restaurant using Local Storage\cite{localstorage}.

\section{User Interface}

\begin{figure}[H]\centering
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/01.png}
        \captionof{figure}{Home page showing the search bar and a slideshow of various foods.}
        \label{fig:01}
    \end{minipage}
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/02.png}
        \captionof{figure}{Log in button in the navigation bar. Logging in and signing up can be done at any point during the navigation.}
        \label{fig:02}
    \end{minipage}
\end{figure}

\begin{figure}[H]\centering
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/03.png}
        \captionof{figure}{Log in modal with sign up prompt.}
        \label{fig:03}
    \end{minipage}
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/04.png}
        \captionof{figure}{Sign up modal showing a link to go back to the log in modal.}
        \label{fig:04}
    \end{minipage}
\end{figure}

\begin{figure}[H]\centering
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/05.png}
        \captionof{figure}{User menu showing user specific navigation links.}
        \label{fig:05}
    \end{minipage}
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/06.png}
        \captionof{figure}{Restaurant owners see a special icon to indicate their elevated role.}
        \label{fig:06}
    \end{minipage}
\end{figure}

\begin{figure}[H]\centering
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/07.png}
        \captionof{figure}{Superusers see a different icon indicating their role, plus an additional navigation item to view and manage upgrade requests (user requests to be promoted to restaurant owners).}
        \label{fig:0}
    \end{minipage}
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/08.png}
        \captionof{figure}{Users can search either by city name or by postal code.}
        \label{fig:08}
    \end{minipage}
\end{figure}

\begin{figure}[H]\centering
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/09.png}
        \captionof{figure}{Search results}
        \label{fig:09}
    \end{minipage}
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/10.png}
        \captionof{figure}{Restaurant page viewed as a normal user.}
        \label{fig:10}
    \end{minipage}
\end{figure}

\begin{figure}[H]\centering
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/11.png}
        \captionof{figure}{Restaurant page viewed as the restaurant owner or as a superuser.}
        \label{fig:11}
    \end{minipage}
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/12.png}
        \captionof{figure}{Restaurant owner editing their restaurant details.}
        \label{fig:12}
    \end{minipage}
\end{figure}

\begin{figure}[H]\centering
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/13.png}
        \captionof{figure}{Restaurant edit page: changing the logo and cover image; adding and removing menu items.}
        \label{fig:13}
    \end{minipage}
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/14.png}
        \captionof{figure}{Modal for adding menu items.}
        \label{fig:14}
    \end{minipage}
\end{figure}

\begin{figure}[H]\centering
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/15.png}
        \captionof{figure}{When the user adds items to their basket, a basket button appears, showing the number of items added.}
        \label{fig:15}
    \end{minipage}
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/16.png}
        \captionof{figure}{If the user doesn't have a valid address in the same area (defined by the postal code) as the restaurant, they will not be able to checkout and will be shown this message. The basket will still be saved in local storage, so the user can add a valid address, go back to the restraurant page and complete the order.}
        \label{fig:16}
    \end{minipage}
\end{figure}

\begin{figure}[H]\centering
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/17.png}
        \captionof{figure}{User account page, here users can add and remove addresses, change passwords and fill an upgrade request.}
        \label{fig:17}
    \end{minipage}
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/18.png}
        \captionof{figure}{If the user has a valid address, the checkout can be completed.}
        \label{fig:18}
    \end{minipage}
\end{figure}

\begin{figure}[H]\centering
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/19.png}
        \captionof{figure}{On checkout, a new order is submitted, and the user is sent to the order page. This page will automatically update the status asynchronously every 30 seconds. This page is also accessible from the user orders page.}
        \label{fig:19}
    \end{minipage}
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/20.png}
        \captionof{figure}{The user orders page shows the history of all orders, and allows them to view the relative details.}
        \label{fig:20}
    \end{minipage}
\end{figure}

\begin{figure}[H]\centering
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/21.png}
        \captionof{figure}{The restaurant owner can see every order submitted to their restaurant, and view the relative details.}
        \label{fig:21}
    \end{minipage}
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/22.png}
        \captionof{figure}{The restaurant owner can change the status of orders. If the order is accepted, it becomes pending, if rejected it becomes cancelled. A pending order can then be either completed or cancelled.}
        \label{fig:22}
    \end{minipage}
\end{figure}

\begin{figure}[H]\centering
    \begin{minipage}{.45\textwidth}\centering
        \includegraphics[width=.85\linewidth]{screenshots/23.png}
        \captionof{figure}{Once an order is completed or cancelled, its status cannot be changed anymore.}
        \label{fig:23}
    \end{minipage}
\end{figure}


\section{Architecture}

\begin{figure}[H]\centering
    \includegraphics[width=\textwidth]{nav_graph.pdf}
    \captionof{figure}{Navigation graph of the website. \texttt{ROO}: Restaurant Owner Only; \texttt{SO}: Superuser Only.}
    \label{fig:nav_graph}
\end{figure}

\begin{figure}[H]\centering
    \includegraphics[width=\textwidth]{data_graph.pdf}
    \captionof{figure}{Data graph of the website.}
    \label{fig:data_graph}
\end{figure}

\section{Performance Analysis}

\begin{figure}[H]\centering
    \includegraphics[width=\textwidth]{lighthouse.pdf}
    \captionof{figure}{Lighthouse report}
    \label{fig:lighthouse}
\end{figure}

Performance analysis through Lighthouse\cite{lighthouse_docs} indicates good overall scores through all the website pages.

The most effective load time optimization was enabling preloading for big images, CSS and JavaScript files.

The list of resources to preload is partially passed to the template engine on the fly, and added dynamically in case of user-generated content like restaurant logos and cover images.

\section{Significant Code Fragments}

\subsection{Hashing and Verifying Passwords, Generating JWT}

\begin{minted}[fontsize=\footnotesize]{typescript}
export async function hash_password(password: string): Promise<string> {
    let salt = crypto.randomBytes(16);
    return await argon2.hash(password, {
        ...HASH_CONF,
        salt,
    })
}
 
export async function verify_password(
        password: string, hash: string
): Promise<boolean> {
    return await argon2.verify(hash, password, HASH_CONF);
}

export function generate_jwt(_id: string): string {
    return sign({_id: _id}, SECRET, {expiresIn: JWT_EXPIRATION_MS})
}
\end{minted}

\subsection{Definition of the User Model}

\begin{minted}[fontsize=\footnotesize]{typescript}
import {Schema, model, Document} from 'mongoose'
import {AddressObj} from './address'

export interface IUser extends Document {
    is_admin: boolean,
    is_superuser: boolean,
    username: string,
    pw_hash: string,
    addresses: Array<AddressObj>,
}

const UserSchema = new Schema<IUser>({
    is_admin: {type: Boolean, default: false},
    is_superuser: {type: Boolean, default: false},
    username: {type: String, required: true, unique: true},
    pw_hash: {type: String, required: true},
    addresses: {type: [{
        country: {type: String}, address: {type: String},
        city: {type: String}, postal_code: {type: String},
        intercom: {type: String}
    }], required: false},
})

export const User = model<IUser>('User', UserSchema)
\end{minted}

\subsection{Reusable Form Template}

\begin{minted}[fontsize=\footnotesize,breaklines=true]{html}
<% { %>
<form id="<%= _form.id %>" <% if (_form.onchange) { %>onChange="<%= _form.onchange %>"<% } %>>
    <% _form.fields.forEach(f => { %>
        <% let fid = f.id || (f.name + 'Entry') %>
        <div>
            <label for="<%= fid %>"><%= f.label %></label>
            <% if (f.type == 'textarea') { %>
                <textarea name="<%= f.name %>" id="<%= fid %>"></textarea>
            <% } else { %>
                <input type="<%= f.type == 'number' ? 'text' : f.type || 'text' %>" name="<%= f.name %>"
                    id="<%= fid %>"
                    <% if (f.type == 'number') { %>
                        oninput="(function(e) {e.value = Array.from(e.value).filter(c => '1234567890.'.includes(c)).join('')})(this)"
                    <% } %>
                    <% if (f.value) { %>
                        value="<%= f.value %>"
                    <% } %>
                >
            <% } %>
            <% if (f.err) { %>
                <div class="invalid-feedback"><%- f.err %></div>
            <% } %>
        </div>
    <% }) %>
    <div id="<%= _form.id %>Err" class="error">An error occurred</div>
    <% if (_form.btn) { %>
        <button type="button" id="<%= _form.btn.id %>"
            class="<%= _form.btn.cls || '' %>" onclick="<%= _form.btn.onclick %>">
            <%= _form.btn.text %>
        </button>
    <% } %>
</form>
<% } %>
\end{minted}

\subsection{Automatic Order Status Update}

\begin{minted}[fontsize=\footnotesize]{javascript}
function updateStatus() {
    fetch(
        '/order/<%= order._id.toString() %>/status', {method: 'GET'}
    ).then(res => {
        if (res.ok) return res.json()
        throw Error(res.statusText)
    }).then(res => {
        document.getElementById('statusDisplay').innerText = res.status
        if (res.status == 'Cancelled' || res.status == 'Completed') {return}
        setTimeout(() => updateStatus(), 30000)  // update every 30 seconds
    }).catch(err => {
        console.error(err)
    })
}

setTimeout(() => updateStatus(), 30000)  // update every 30 seconds
\end{minted}

\subsection{SCSS Home Page Slideshow}

\begin{minted}[fontsize=\footnotesize,breaklines=true]{scss}
@use "sass:math";

$jumbotronAnimationTime: 30s;
$jumbotronBaseDelay: math.div($jumbotronAnimationTime, 5);
.jumbotron {
    background-color: black;
    height: 600px;
    max-height: 70vh;
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
    position: relative;
    .im0, .im1, .im2, .im3, .im4 {
        width: 100%; height: 100%;
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
        position: absolute; top: 0; bottom: 0; left: 0; right: 0;
        animation: jumbotronImgFade $jumbotronAnimationTime ease infinite;
    }
    .im0 {
        background-image: url('/img/jumbotron/0.webp');
        animation-delay: ($jumbotronBaseDelay*1);
    }
    .im1 {
        background-image: url('/img/jumbotron/1.webp');
        animation-delay: ($jumbotronBaseDelay*2);
    }
    .im2 {
        background-image: url('/img/jumbotron/2.webp');
        animation-delay: ($jumbotronBaseDelay*3);
    }
    .im3 {
        background-image: url('/img/jumbotron/3.webp');
        animation-delay: ($jumbotronBaseDelay*4);
    }
    .im4 {
        background-image: url('/img/jumbotron/4.webp');
        animation-delay: ($jumbotronBaseDelay*5);
    }
}

@keyframes jumbotronImgFade {
    0%   {opacity: 1;}
    10%  {opacity: 0;}
    20%  {opacity: 0;}
    30%  {opacity: 0;}
    40%  {opacity: 0;}
    50%  {opacity: 0;}
    60%  {opacity: 0;}
    70%  {opacity: 0;}
    80%  {opacity: 0;}
    90%  {opacity: 1;}
    100% {opacity: 1;}
}
\end{minted}

\section{Future Improvements}

Given this project has been developed as a University project, its feature set is limited. It can be considered as a proof of concept implementation or a minimum viable product.

While it can definitely be deployed in production and provide real value in its current state (according to the author's estimate), it could be further improved with additional features and architectural changes. Below are some suggestions for future developments:

\begin{itemize}
    \item Switch to a SPA (Single Page Application) model: using modern frontend frameworks such as Angular\cite{angular}, Vue.js\cite{vue} or React\cite{react} and decoupling the frontend from the backend would result in a better user experience, maintainability and code reusability potential.
    \item Consider a different DBMS: while MongoDB is certainly a very good DBMS, it's possible that this kind of application would benefit from using one following a more traditional relational model.
    \item Add payment providers: it would be possible to integrate a variety of different payment providers, like Stripe\cite{stripe} or PayPal\cite{paypal}, allowing users to pay for their orders digitally.
    \item Add a review system: this would allow users to identify certain characteristics they are looking for, and others they want to avoid, helping them to narrow down their choices.
    \item Add open times to restaurants: if a restaurant is closed a certain day of the week, or if they're closed for holidays the user wouldn't be able to place an order.
    \item Add delivery times: right now the preferred delivery time can be indicated in the order notes, but adding a proper database row would be easier to work with.
    \item Implement an extras system: it would be nice to allow users to add extra ingredients to their orders.
    \item Implement media files conversion and size constraints: converting the user uploaded images to web friendly formats like WebP, and scaling them to standard sizes, would further improve performance and reduce the page load time. To avoid overloading the server with work, it would still be advisable to introduce a maximum size cap for uploaded media files: this way the server will avoid working on files that are too big and would require too much processing time.
\end{itemize}

\printbibliography

\end{document}
