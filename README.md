# Ammuccamu

## Configuration

You can configure Ammuccamu via environment variables. You can set these variables either manually or by adding them to the `.env` file.

| Variable      | Default                         |
|---------------|---------------------------------|
| `AMC_PORT`    | `3000`                          |
| `AMC_DB`      | `mongodb://localhost:27017/amc` |
| `AMC_APPNAME` | `Ammuccamu`                     |
| `SECRET`      | N/A; required for JWT signing   |
